<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 06.07.16
 * Time: 18:31
 */

class DublSeeker{

    private $path;
    private $fileToSave;
    private $filesHashes = [];

    public function __construct($path, $fileToSave){

        $this->path = $path;
        $this->fileToSave = $fileToSave;
    }

    public function run(){

        $this->makeHashes();
        $this->putDataInFile();
    }

    private function scopeDir(){

        $files = [];

        $iter = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->path, RecursiveDirectoryIterator::SKIP_DOTS)
        );

        foreach ($iter as $path => $dir) {
            if (!$dir->isDir()) {
                $files[] = $path;
            }
        }

        return $files;
    }

    private function makeHashes(){

        foreach($this->scopeDir() as $file){
            // possible to use system("md5sum ".$file) in *nix for large files
            $fileHash = md5_file($file);

            if(array_key_exists($fileHash, $this->filesHashes)){
                array_push($this->filesHashes[$fileHash], $file);
            }else{
                $this->filesHashes[$fileHash] = [$file];
            }

        }
    }

    private function putDataInFile(){

        file_put_contents($this->fileToSave, '');
        foreach($this->filesHashes as $key => $value){

            if(count($value) > 1){
                foreach($value as $item){
                    file_put_contents($this->fileToSave, $item."\n", FILE_APPEND | LOCK_EX);
                }
            }

            file_put_contents($this->fileToSave, "\n", FILE_APPEND);
        }
    }

}

$dublSeeker = new DublSeeker('/home/nick/test', '/home/nick/result.txt');
$dublSeeker->run();